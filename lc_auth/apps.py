from django.apps import AppConfig


class LcAuthConfig(AppConfig):
    name = 'lc_auth'
