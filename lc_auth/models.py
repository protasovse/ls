from django.contrib.auth.models import User, UserManager, Group, AbstractUser


class LcUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related('groups')


class Author(AbstractUser):

    objects = LcUserManager()

    @property
    def is_expert(self):
        return self.groups.filter(name='expert').exists()

    @property
    def is_user(self):
        return self.groups.filter(name='user').exists()

    def add_to_experts(self):
        group = Group.objects.get_by_natural_key('expert')
        group.user_set.add(self)

    def add_to_users(self):
        group = Group.objects.get_by_natural_key('user')
        group.user_set.add(self)
