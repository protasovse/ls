from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from lc_auth.models import Author


@admin.register(Author)
class AuthorAdmin(UserAdmin):
    pass
