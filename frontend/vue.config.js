module.exports = {
  transpileDependencies: [
    'vuetify',
  ],

  runtimeCompiler: true,

  css: {
    sourceMap: true,
  },
};
