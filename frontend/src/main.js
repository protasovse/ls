import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';
import VueBus from 'vue-bus';
import App from './App.vue';
import router from './router';
import { createProvider } from './plugins/vue-apollo';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

// Плагины

// Native websocket implementation (https://github.com/nathantsoi/vue-native-websocket)
Vue.use(
  VueNativeSock,
  `${process.env.VUE_APP_WEBSOCKET}`,
  { format: 'json', connectManually: true },
);

// A event bus for Vue.js (https://github.com/yangmingshan/vue-bus)
Vue.use(VueBus);

new Vue({
  router,
  apolloProvider: createProvider(),
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
