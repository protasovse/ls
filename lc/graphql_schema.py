import graphene
import graphql_jwt
from graphene_django.debug import DjangoDebug

import questions.graphql.schema


class Query(
    questions.graphql.schema.Query,
    graphene.ObjectType,
):
    debug = graphene.Field(DjangoDebug, name='_debug')


class JWTMutation(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Mutation(
    JWTMutation,
    questions.graphql.schema.Mutation,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(
    query=Query,
    mutation=Mutation
)
