import questions.routing
from channels.routing import ProtocolTypeRouter, URLRouter

from questions.middleware import TokenAuthMiddleware

application = ProtocolTypeRouter({
    'websocket': TokenAuthMiddleware(
        URLRouter(questions.routing.websocket_urlpatterns)
    ),
})
