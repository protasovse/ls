from django.utils.translation import gettext_lazy


def _(*args, **kwargs):
    return gettext_lazy(*args, **kwargs)
