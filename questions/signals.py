from django.db.models.signals import post_save
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.dispatch import receiver

from .models import Question


@receiver(post_save, sender=Question, dispatch_uid='new_question')
def new_question(sender, instance, created, **kwargs):
    if created:
        message = {
            'type': 'response.proxy',
            'data': {
                'id': instance.id,
                'create_at': str(instance.create_at),
                'title': instance.title,
            },
            'event': 'new.question',
        }
        async_to_sync(get_channel_layer().group_send)('questions', message)
