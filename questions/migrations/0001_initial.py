# Generated by Django 3.0.5 on 2020-04-04 07:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                ('deleted', models.BooleanField(default=False, verbose_name='deleted')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answers', to=settings.AUTH_USER_MODEL, verbose_name='author')),
            ],
            options={
                'verbose_name': 'answer thread',
                'verbose_name_plural': 'answers threads',
                'permissions': (('view_answer_on_own_question', 'Видеть только ответы на свои вопросы'),),
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                ('deleted', models.BooleanField(default=False, verbose_name='deleted')),
                ('title', models.CharField(max_length=255, verbose_name='question')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to=settings.AUTH_USER_MODEL, verbose_name='author')),
            ],
            options={
                'verbose_name': 'question',
                'verbose_name_plural': 'questions',
                'permissions': (('view_own_question', 'Видеть только свои вопросы'), ('change_own_question', 'Редактировать только свои вопросы'), ('delete_own_question', 'Удалить только свои вопросы')),
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                ('deleted', models.BooleanField(default=False, verbose_name='deleted')),
                ('text', models.TextField(verbose_name='message text')),
                ('answer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='questions.Answer', verbose_name='answer thread')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='author')),
                ('to_message', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='questions.Message', verbose_name='reply to')),
            ],
            options={
                'verbose_name': 'message',
                'verbose_name_plural': 'messages',
                'permissions': (('view_message_on_own_question', 'Видеть только сообщения на свои вопросы'), ('add_message_on_own_question', 'Добавлять только сообщения на свои вопросы'), ('change_own_message', 'Редактировать только свои сообщения'), ('delete_own_message', 'Удалить только свои сообщения')),
            },
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answers', to='questions.Question', verbose_name='question'),
        ),
    ]
