from django.contrib import admin

from lc_auth.models import Author
from questions.models import Question, Answer, Message


@admin.register(Question, Answer, Message)
class PersonAdmin(admin.ModelAdmin):
    pass
