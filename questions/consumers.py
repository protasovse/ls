from channels.generic.websocket import AsyncJsonWebsocketConsumer


class BaseConsumer(AsyncJsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = self.scope['user']

    async def connect(self):
        await self.accept()
        if self.user.is_anonymous:
            await self.throw_error('Авторизация не удалась')
            await self.close(code=1000)

    async def disconnect(self, close_code):
        await self.send_notice('Разъединено…')
        await super().disconnect(close_code)

    async def send_message(self, data: dict, event: str = ''):
        """ Отправляет сообщение клиенту """
        await self.send_json(content={
            'status': 'ok',
            'data': data,
            'event': event,
        })

    async def send_notice(self, msg: str, event: str = ''):
        """ Отправляет техническое уведомление клиенту """
        await self.send_json(content={
            'status': 'notice',
            'msg': msg,
            'event': event,
        })

    async def throw_error(self, msg: str, event: str = ''):
        """ Отправляет ошибку клиенту """
        await self.send_json(content={
            'status': 'error',
            'msg': msg,
            'event': event,
        })

    async def receive(self, **kwargs):
        try:
            await super().receive(**kwargs)
        except ValueError as e:
            await self.throw_error(f'Ошибка парсинга JSON: «{e}»')

    async def receive_json(self, content, **kwargs):
        content = await self.parse_content(content)
        if not content:
            await self.throw_error('Invalid message, must be: {"event": str, "data": dict}')
            return

        event = content['event'].replace('.', '_')
        method = getattr(self, f'event_{event}', None)
        if method:
            await method(content)
        else:
            await self.throw_error('Unknown event', event=content['event'])

    async def join_group(self, group_name: str):
        self.groups.append(group_name)
        await self.channel_layer.group_add(group_name, self.channel_name)
        await self.send_notice(f'Join to group: {group_name}')

    async def group_send(self, group_name: str, data: dict, event: str = ''):
        if group_name in self.groups:
            message = {'type': 'response.proxy', 'data': data, 'event': event}
            await self.channel_layer.group_send(group_name, message)

    async def response_proxy(self, data: dict):
        await self.send_message(data['data'], event=data.get('event'))

    @staticmethod
    async def parse_content(content) -> dict or None:
        """
        Проверка формата полученного сообщения content: {'event': str, 'data': dict}
        """
        if isinstance(content, dict) and isinstance(content.get('event'), str) and \
                isinstance(content.get('data'), dict):
            return content


class QuestionConsumer(BaseConsumer):
    async def connect(self):
        await super().connect()
        # TODO: Поставить условие, что если пользователь — эксперт
        await self.join_group('questions')
