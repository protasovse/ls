from channels.auth import UserLazyObject
from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from graphql_jwt.utils import jwt_decode
from jwt import InvalidTokenError


@database_sync_to_async
def get_user(token):
    try:
        decoded_data = jwt_decode(token)
        query = {
            get_user_model().USERNAME_FIELD: decoded_data[get_user_model().USERNAME_FIELD],
        }
        return get_user_model().objects.filter(**query).first()
    except InvalidTokenError:
        return AnonymousUser()


class TokenAuthMiddleware(BaseMiddleware):
    """
    """
    def populate_scope(self, scope):
        if 'user' not in scope:
            scope['user'] = UserLazyObject()

    async def resolve_scope(self, scope):
        token = scope['query_string'].decode('utf8')
        scope['user']._wrapped = await get_user(token)
