"""
Модели
"""
from django.db import models
from django.contrib.auth import get_user_model


class RecordManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)


class BaseRecord(models.Model):
    """ Базовый класс для всех типов записей """
    create_at = models.DateTimeField('creation date', auto_now_add=True)
    deleted = models.BooleanField('deleted', default=False)

    objects = models.Manager()
    published = RecordManager()

    class Meta:
        abstract = True


class Question(BaseRecord):
    """ Вопрос """
    title = models.CharField('question', max_length=255)
    author = models.ForeignKey(
        get_user_model(), verbose_name='author', on_delete=models.CASCADE,
        related_name='questions')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'question'
        verbose_name_plural = 'questions'
        permissions = (
            ('view_own_question', 'Видеть только свои вопросы'),
            ('change_own_question', 'Редактировать только свои вопросы'),
            ('delete_own_question', 'Удалить только свои вопросы'),
        )


class Answer(BaseRecord):
    """ Ветки ответов (обсуждения), участники обсуждения """
    question = models.ForeignKey(
        Question, verbose_name='question', on_delete=models.CASCADE,
        related_name='answers')
    author = models.ForeignKey(
        get_user_model(), verbose_name='author', on_delete=models.CASCADE,
        related_name='answers')

    class Meta:
        verbose_name = 'answer thread'
        verbose_name_plural = 'answers threads'
        permissions = (
            ('view_answer_on_own_question',
             'Видеть только ответы на свои вопросы'),
        )


class Message(BaseRecord):
    """ Сообщения обсуждения """
    answer = models.ForeignKey(
        Answer, verbose_name='answer thread', on_delete=models.CASCADE,
        related_name='messages')

    author = models.ForeignKey(
        get_user_model(), verbose_name='author', on_delete=models.CASCADE,
        related_name='+')

    text = models.TextField('message text')
    to_message = models.ForeignKey(
        'self', verbose_name='reply to', on_delete=models.CASCADE,
        null=True, related_name='+')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'message'
        verbose_name_plural = 'messages'
        permissions = (
            ('view_message_on_own_question',
             'Видеть только сообщения на свои вопросы'),
            ('add_message_on_own_question',
             'Добавлять только сообщения на свои вопросы'),
            ('change_own_message', 'Редактировать только свои сообщения'),
            ('delete_own_message', 'Удалить только свои сообщения'),
        )
