from django.contrib.auth import get_user_model


def username_exists(username):
    return get_user_model().objects.only('id').filter(
        **{get_user_model().USERNAME_FIELD: username}).exists()
