import graphene
from graphql_jwt.decorators import login_required

from questions.graphql.mutations import QuestionCreateMutation, SignUp, \
    MessageCreateMutation
from questions.graphql.pagination import get_paginator
from questions.graphql.types import QuestionPaginatedType
from questions.models import Question


class Query(graphene.ObjectType):
    questions = graphene.Field(
        QuestionPaginatedType, page=graphene.Int(), id=graphene.ID()
    )

    @login_required
    def resolve_questions(self, info, page=0, id=None):
        page_size = 10
        user = info.context.user

        if user.has_perm('questions.view_question'):
            qs = Question.objects.all()
        elif user.has_perm('questions.view_own_question'):
            qs = Question.objects.filter(author=user)
        else:
            return {}

        if id:
            qs = qs.filter(id=id)

        return get_paginator(qs, page_size, page, QuestionPaginatedType)


class Mutation(graphene.ObjectType):
    """ Мутации """
    question_create = QuestionCreateMutation.Field()
    message_create = MessageCreateMutation.Field()
    sign_up = SignUp.Field()
