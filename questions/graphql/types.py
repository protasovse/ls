import graphene
from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType

from .pagination import Paginated
from ..models import Question, Answer, Message


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        only_fields = ('id', 'username', 'email', 'first_name', 'last_name')


class QuestionType(DjangoObjectType):
    class Meta:
        model = Question
        only_fields = ('id', 'title', 'create_at', 'author', 'answers')
        filter_fields = ('id', )


class QuestionPaginatedType(graphene.ObjectType):
    class Meta:
        interfaces = (Paginated,)

    objects = graphene.List(QuestionType)


class AnswerType(DjangoObjectType):
    class Meta:
        model = Answer


class MessageType(DjangoObjectType):
    class Meta:
        model = Message

