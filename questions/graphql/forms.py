from django import forms

from questions.models import Question


class CurrentUserModelForm(forms.ModelForm):
    """ Для передачи user из request """
    def __init__(self, *args, **kwargs):
        self.author = kwargs.pop('author', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        if self.author:
            self.instance.author = self.author
        return super().save(commit)


class QuestionModelForm(CurrentUserModelForm):
    class Meta:
        model = Question
        fields = ('title',)
