from enum import Enum

import graphene
from django.contrib.auth import get_user_model
from django.db import transaction

from questions.graphql.types import QuestionType, UserType, MessageType
from questions.graphql.utils import username_exists
from questions.models import Question, Answer, Message


class QuestionCreateMutation(graphene.Mutation):
    class Arguments:
        title = graphene.String(required=True)

    errors = graphene.List(graphene.String)
    question = graphene.Field(QuestionType)

    @classmethod
    def mutate(cls, root, info, title):
        user = info.context.user

        if user.is_anonymous or not user.has_perm('questions.add_question'):
            return cls(errors=['Is not permitted.'])

        if len(title) < 15:
            return cls(
                errors=['Short question, must be more than 15 characters.'])

        return cls(question=Question.objects.create(title=title, author=user))


class TypeArg(Enum):
    ON_QUESTION = 1
    ON_ANSWER = 2


class MessageCreateMutation(graphene.Mutation):
    class Arguments:
        type = graphene.Argument(
            graphene.Enum.from_enum(TypeArg), required=True)
        type_id = graphene.ID(required=True)
        text = graphene.String(required=True)
        to_message_id = graphene.ID()

    errors = graphene.List(graphene.String)
    message = graphene.Field(MessageType)

    @classmethod
    @transaction.atomic()
    def mutate(cls, root, info, type, **kwargs):  # todo: restrict permissions
        user = info.context.user
        if user.is_anonymous:
            return cls(errors=['Is not permitted.'])

        method = getattr(
            cls, f'create_message_{TypeArg(type).name.lower()}', None)

        if method:
            message = method(user, **kwargs)
            return MessageCreateMutation(message=message)

        return MessageCreateMutation(errors=['The message type is invalid.'])

    @staticmethod
    def create_message_on_question(author, **kwargs):
        answer = Answer(question_id=kwargs['type_id'])
        answer.save()
        message = Message(author=author, answer=answer, text=kwargs['text'])
        message.save()
        return message

    @staticmethod
    def create_message_on_answer(author, **kwargs):
        message = Message(
            author=author,
            answer_id=kwargs['type_id'],
            text=kwargs['text'],
            to_message_id=kwargs.get('to_message_id', None)
        )
        message.save()
        return message


class SignUp(graphene.Mutation):
    class Arguments:
        is_expert = graphene.Boolean(required=True)
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String()
        first_name = graphene.String()
        last_name = graphene.String()

    user = graphene.Field(UserType)

    @classmethod
    def mutate(cls, root, info, is_expert, **kwargs):
        assert not username_exists(kwargs['username']),\
            'This username is already in use.'

        user = get_user_model().objects.create_user(**kwargs)

        if is_expert:
            user.add_to_experts()
        else:
            user.add_to_users()

        return cls(user=user)
