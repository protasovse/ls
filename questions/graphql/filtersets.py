import django_filters


class QuestionsListFilterSet(django_filters.FilterSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.request.user.has_perm('questions.view_question'):
            pass
        elif self.request.user.has_perm('questions.view_own_question'):
            self.queryset = self.queryset.filter(author=self.request.user)
        else:
            self.queryset = self.queryset.none()
